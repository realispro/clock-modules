package clock.impl;

import clock.api.Clock;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ClockService implements Clock{
    @Override
    public String getFormattedTime() {
        return DateTimeFormatter.ISO_DATE_TIME.format(getCurrentTime());
    }

    @Override
    public LocalDateTime getCurrentTime() {
        return LocalDateTime.now();
    }
}
