module clock.impl {

    requires clock.api;
    provides clock.api.Clock with clock.impl.ClockService;
}