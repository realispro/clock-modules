package clock.api;

import java.time.LocalDateTime;

public interface Clock {

    String getFormattedTime();

    LocalDateTime getCurrentTime();
}
