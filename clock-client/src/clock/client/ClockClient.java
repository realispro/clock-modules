package clock.client;

import clock.api.Clock;

import java.util.ServiceLoader;


public class ClockClient {

    public static void main(String[] args) {

        Clock clock = ServiceLoader.load(Clock.class).findFirst().orElseThrow();
        System.out.println("Current time is " + clock.getFormattedTime());
    }
}
